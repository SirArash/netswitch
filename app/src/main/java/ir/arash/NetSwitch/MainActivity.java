package ir.arash.NetSwitch;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.pushpole.sdk.PushPole;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        try {
            super.onCreate(savedInstanceState);
            PushPole.initialize(this, false);

            System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");

            Intent RadioInfo = new Intent("android.intent.action.MAIN");
            RadioInfo.setClassName("com.android.settings", "com.android.settings.RadioInfo");
            startActivity(RadioInfo);
            finish();

        } catch (Exception e) {
            AlertDialog.Builder ErrorMessage = new AlertDialog.Builder(this);
            ErrorMessage.setMessage("متاسفانه دستگاه شما امکان تغییر در تنظیمات را منع کرده است و این برنامه برای شما کارایی ندارد، آیا میخواهید برنامه رو حذف کنیم؟");
            ErrorMessage.setPositiveButton("متوجه ام، حذفش کن!", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    Intent Uninstall = new Intent(Intent.ACTION_DELETE);
                    Uninstall.setData(Uri.parse("package:" + getApplicationContext().getPackageName()));
                    startActivity(Uninstall);

                    finish();
                }
            });

            ErrorMessage.show();
        }
    }
}